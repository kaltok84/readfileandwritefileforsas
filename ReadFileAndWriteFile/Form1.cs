﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadFileAndWriteFile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private bool checkValidPath(string path)
        {
            try
            {
                String goodPath = Path.GetFileName(path);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.StackTrace);
                return false;
            }
            return true;
        }

        private void btnFileOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if(DialogResult.OK == fileDialog.ShowDialog())
            {
                txtOpenFile.Text = fileDialog.FileName;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            const string nameHead = "name=\"";
            if (checkValidPath(txtOpenFile.Text))
            {
                List<string> resultLines = new List<string>();
                string[] lines = File.ReadAllLines(txtOpenFile.Text);
                foreach(string line in lines)
                {
                    if (line.Contains(nameHead))
                    {
                        int startIndex = line.IndexOf(nameHead) + nameHead.Length;
                        int endIndex = line.IndexOf('"', startIndex + 1);
                        string name = line.Substring(startIndex, endIndex - startIndex);

                        int contentSIndex = line.IndexOf('>', endIndex + 1);
                        int contentEIndex = line.IndexOf('<', contentSIndex + 1);
                        string content = line.Substring(contentSIndex + 1, contentEIndex - contentSIndex - 1);

                        string result = "\t<string name=\"" + name + "\" did=\"" + name + "\">" +
                            content + "</string>";
                        resultLines.Add(result);
                    }
                    else
                    {
                        resultLines.Add(line);
                    }                    
                }

                // write file
                using (StreamWriter file = new StreamWriter(txtOpenFile.Text))
                {                    
                    foreach (string line in resultLines)
                    {
                        file.WriteLine(line);
                    }
                }
            }
        }
    }
}
